const { AccessDeniedError } = require('./access-denied');
const { AuthenticationError } = require('./authentication-error');
const { ValidationError } = require('./validation-errors');


module.exports = Object.create({
  AccessDeniedError,
  AuthenticationError,
  ValidationError,
});
