const buildMakePayment = require('./paymentService');

const makePayment = buildMakePayment();

const CONNECTION_STRING = process.env.AMQP_CONNECTION_STRING;
const CHANNEL_NAME = process.env.AMQP_CHANNEL_NAME;
const QUEUE_NAME = process.env.AMQP_QUEUE_NAME;

makePayment(CONNECTION_STRING, CHANNEL_NAME, QUEUE_NAME)
  .then(payment => {
    payment().init();
  })
  .catch(error => {
    throw new Error(error);
  });
