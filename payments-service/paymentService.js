const amqplib = require('amqplib');


module.exports = function buildMakePayment() {

  return async function makePayment(connectionString, channelName, queueName) {

    const connection = await amqplib.connect(connectionString);
    const channel = await connection.createChannel(channelName);

    return function payment () {

      async function acknowledge(message) {
        await channel.ack(message);
      }

      async function onMessageReceived(message) {
        let contents = message.content.toString();
        let parsed = JSON.parse(contents);
        if (parsed.subscription && parsed.plan) {
          let { subscription, plan } = parsed;
          console.log(`Charging card ${subscription.cardNumber} with ${plan.price}`);
        } else {
          console.log('Invalid payload. Aborting...');
        }
        acknowledge(message);
      }

      async function init() {
        console.log(`Initializing RabbitMQ connection to ${connectionString}`);
        // check queue avaibility
        await channel.assertQueue(queueName);
        await channel.consume(queueName, onMessageReceived);

        return Object.freeze({
          connection,
          channel,
        });
      }

      return Object.create({
        init,
      })
    };
  };
}

