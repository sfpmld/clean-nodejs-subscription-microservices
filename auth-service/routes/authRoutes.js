const makeExpressCallback = require('../helpers/express-callback');
const {
  getSignIn,
  getSignUp,
  notFound
} = require('../controllers');


const router = require('express').Router();

// post planRoutes Requests
router.post('/sign-up', makeExpressCallback(getSignUp));

// post planRoutes Requests
router.post('/sign-in', makeExpressCallback(getSignIn));

// notFound
router.use('*', makeExpressCallback(notFound));


module.exports = router;
