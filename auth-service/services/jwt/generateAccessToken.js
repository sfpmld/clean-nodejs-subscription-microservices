module.exports = function makeGenerateAccessToken ({ sign }) {
  return async function generateAccessToken (user = {}) {

    if (!user) {
      throw new Error('Invalid user');
    }
    // delete password field for security purpose
    delete user.password;
    const token = sign(user, process.env.AUTH_SECRET, user.id);

    return token;
  };
};
