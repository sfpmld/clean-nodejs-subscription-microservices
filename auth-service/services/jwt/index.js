const { sign } = require('../../utils/jwt-utils');
const makeGenerateAccessToken = require('./generateAccessToken');

const generateAccessToken = makeGenerateAccessToken({ sign });

module.exports = Object.freeze({
  generateAccessToken,
});
