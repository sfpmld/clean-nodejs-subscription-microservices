const { makeUser } = require('../../entities');

module.exports = function makeCreateUser(UserDb, hash) {

  async function hashPassword(password) {
    return await hash(password);
  }

  return async function createUser(user) {

    let userToCreate = makeUser(user);

    user.password = await hashPassword(user.password);
    userToCreate = {
      id: userToCreate.getId(),
      firstName: userToCreate.getFirstName(),
      lastName: userToCreate.getLastName(),
      email: userToCreate.getEmail(),
      password: user.password,
      createdAt: userToCreate.getCreatedAt(),
      updatedAt: userToCreate.getUpdatedAt()
    };

    UserDb.create(userToCreate);
    return userToCreate;
  };
};
