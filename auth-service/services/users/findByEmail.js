module.exports = function makeFindByEmail({ UserDb }) {
  return async function findByEmail({ email }) {
    return await User.findOne({ where: { email } });
  }
}
