const { UserDb } = require('../../data-access');
const { hash, check } = require('../../utils/bcrypt-utils');
const { generateAccessToken } = require('../../services/jwt');
const { makeUser, makeLoginUser } = require('../../entities/user');

const makeCreateUser = require('./create');
const makeFindByEmail = require('./findByEmail');
const makeSignIn = require('./signIn');
const makeSignUp = require('./signUp');

const createUser = makeCreateUser(UserDb, hash);
const findByEmail = makeFindByEmail({ UserDb });
const signIn = makeSignIn({ findByEmail, check, generateAccessToken });
const signUp = makeSignUp(createUser, generateAccessToken);

module.exports = Object.freeze({
  createUser,
  findByEmail,
  signIn,
  signUp
});
