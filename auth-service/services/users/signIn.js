module.exports = function makeSignIn({ findByEmail, check, generateAccessToken }) {

  function checkPassword(password, hashPassword) {
    return check(password, hashPassword);
  }

  return async function signIn({ email, password }) {

    let user = await findByEmail({ email });

    if(!user) {
      return null;
    };

    if(checkPassword(password, user.password)) {
      return generateAccessToken(user.toJSON());
    }

    // if bad password
    return null;
  };
}
