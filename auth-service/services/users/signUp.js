module.exports = function makeSignUp(createUser, generateAccessToken) {

  return async function signUp({ user }) {
    let token;

    try {
      const userCreated = await createUser(user);
      if(userCreated) {
        token = await generateAccessToken(userCreated);
      }
    } catch (e) {
      throw e
    }

    return token;
  };
};
