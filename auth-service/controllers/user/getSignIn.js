const {
  HTTP_OK,
  HTTP_BAD_REQUEST
} = require('../../nomenclatures/httpStatusCode');
const AuthenticationError = require('../../../errors');

module.exports = function makeGetSignIn ({ signIn }) {
  return async function getSignIn (httpRequest) {

    const headers = {
      'Content-Type': 'application/json'
    };
    const { email, password } = httpRequest.body;
    try {
      const token = await signIn({ email, password });
      if (!token) {
        throw new AuthenticationError('Invalid credentials');
      }
      return {
        headers,
        statusCode: HTTP_OK,
        body: token
      };
    } catch (e) {
      console.log(e);
      return {
        headers,
        statusCode: HTTP_BAD_REQUEST,
        body: {
          error: e.message
        }
      };
    };
  };
};
