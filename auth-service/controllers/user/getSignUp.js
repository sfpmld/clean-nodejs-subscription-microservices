const {
  HTTP_OK,
  HTTP_BAD_REQUEST
} = require('../../nomenclatures/httpStatusCode');

module.exports = function makeGetSignUp ({ signUp }) {
  return async function getSignUp (httpRequest) {
    const headers = {
      'Content-Type': 'application/json'
    };
    try {
      const token = await signUp({
        user: httpRequest.body
      });
      return {
        headers,
        statusCode: HTTP_OK,
        body: token
      };
    } catch (e) {
      console.log(e);
      return {
        headers,
        statusCode: HTTP_BAD_REQUEST,
        body: {
          error: e.message
        }
      };
    };
  };
};
