const {
  signUp,
  signIn
} = require('../services/users');

const notFound = require('./notFound');

const makeGetSignUp = require('./user/getSignUp');
const makeGetSignIn = require('./user/getSignIn');

const getSignUp = makeGetSignUp({ signUp });
const getSignIn = makeGetSignIn({ signIn });

module.exports = Object.create({
  //user
  getSignIn,
  getSignUp,
  // not found
  notFound
});
