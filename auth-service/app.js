const express = require('express');
const router = express.Router();
const app = express();

const api_prefix = require('./config')['API_PREFIX']

const commonMiddlewares = require('../middlewares/common-middlewares');
const mainErrorHandler = require('../middlewares/errors-handler');

const authRoutes = require('./routes/authRoutes');



// Common Middlewares
commonMiddlewares(app);

// Routes Middlewares
app.use('', authRoutes);

// Main Error Handling Block
mainErrorHandler(app)


module.exports = app;

