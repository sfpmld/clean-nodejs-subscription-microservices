const Joi = require('@hapi/joi');
'use strict';

const UserValidationSchema = Joi.object().keys({
  id: Joi.string().guid().required(),
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  email: Joi.string().required(),
  password: Joi.string().required().min(6).max(32),
});

const LoginValidationSchema = Joi.object().keys({
  email: Joi.string().email().required,
  password: Joi.string().required()
})


const User = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      allowNull: false,
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {});
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};

module.exports = Object.freeze({
  User,
  UserValidationSchema,
  LoginValidationSchema
});
