const { User } = require('./user');
const { UserValidationSchema } = require('./user');


module.exports = Object.freeze({
  User,
  UserValidationSchema,
});
