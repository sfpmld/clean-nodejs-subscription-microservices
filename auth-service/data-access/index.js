const makeDbConnection = require('./db/connection');
const { Sequelize, SequelizeClient } = makeDbConnection();

const makeUserModel = require('../models').User;

const makeUserDb = require('./userDb');

const makeDb = () => {
  User = makeUserModel(SequelizeClient, Sequelize);

  return Object.freeze({
    User
  })
}

const UserDb = makeUserDb(makeDb);

module.exports = Object.freeze({
  UserDb
});
