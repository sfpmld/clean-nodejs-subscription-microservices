const { ValidationError } = require('../../errors');

module.exports = function makeUserDb(makeDb) {

  const db = makeDb();

  const findAll = async function() {
    return await db.User.findAll();
  };

  const findOne = async function(id) {
    return await db.User.findOne(id);
  }

  const create = async function(data) {
    return await db.User.create(data)
  }

  const deleteOne = async function(id) {

    const userToDelete = await db.User.findOne({ id });
    return await userToDelete.destroy();
  }

  return Object.freeze({
    findAll,
    findOne,
    create,
    deleteOne
  });
};




