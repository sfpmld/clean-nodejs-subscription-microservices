const Sequelize = require('sequelize');

const DB_HOST = require('../../config')['host'];
const DB_NAME = require('../../config')['database'];
const DB_USERNAME = require('../../config')['username'];
const DB_USER_PASSWORD = require('../../config')['password'];
const DB_PORT = require('../../config')['port'];
const DB_DIALECT = require('../../config')['dialect'];
// const DB_OPERATORS_ALIASES = require('../config')['operatorsaliases'];

const SequelizeClient = new Sequelize(`${DB_DIALECT}://${DB_USERNAME}:${DB_USER_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_NAME}`);

module.exports = function makeDbConnection() {
  SequelizeClient
    .authenticate()
    .then(() => {
      console.log('Database Connection has been established successfully to UserDB.');
      // if connected syncing migration with database
      // SequelizeClient
      //   .sync()
      //   .then(() => {
      //     console.log('Migration launched with success')
      //   });
    })
    .catch(err => {
      console.error('Unable to connect to the database:', err);
    });

  return Object.freeze({
    Sequelize,
    SequelizeClient,
  });
}

