// Serializer function based on sequelize toJSON method
module.exports = function serializer(obj) {
  return JSON.stringify(obj);
}
