const app = require('./app');

const port = require('./config')['PORT'];


const start = app => {
  return new Promise((resolve, reject) => {
    try {
      const server = app.listen(port, () => {
        resolve(server);
      })
    } catch (err) {
      reject(err);
    }
  })
}


// Starting Server
start(app)
  .then(() => {
    console.log(`Auth Service Listening on port ${port}`)
  })
  .catch(err => console.log(`Auth Service start failed: ${err}`));
