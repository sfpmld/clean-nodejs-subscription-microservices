const validate = require('../helpers/validate');
const Id = require('../utils/makeId');
const buildMakeUser = require('./user');
const buildMakeLoginUser = require('./user');


const makeUser = buildMakeUser({ Id, validate });
const makeLoginUser = buildMakeLoginUser({ validate });

module.exports = {
  makeUser,
  makeLoginUser
};
