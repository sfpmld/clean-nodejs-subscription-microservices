module.exports = function buildMakeUser ({ Id, validate }) {
  return function makeUser ({
    id = Id(),
    firstName,
    lastName,
    email,
    password,
    createdAt = Date.now(),
    updatedAt = Date.now()
  } = {}) {

    const validateUser = (function() {
      return validate(
        'User',
        {
          id,
          firstName,
          lastName,
          email,
          password
        },
        'default'
      )
    })();

    if (validateUser.error) {
      throw new Error(validateUser.error.message);
    }

    return Object.freeze({
      getId: () => id,
      getFirstName: () => firstName,
      getLastName: () => lastName,
      getEmail: () => email,
      getPassword: () => password,
      getCreatedAt: () => createdAt,
      getUpdatedAt: () => updatedAt
    });
  }
};
