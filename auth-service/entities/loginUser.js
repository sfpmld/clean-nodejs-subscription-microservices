module.exports = function buildMakeLoginUser ({ validate }) {
  return function makeLoginUser ({
    email,
    password,
  } = {}) {

    const validateUser = (function() {
      return validate(
        'User',
        {
          email,
          password
        },
        'login'
      )
    })();

    if (validateUser.error) {
      throw new Error(validateUser.error.message);
    }

    return Object.freeze({
      getEmail: () => email,
      getPassword: () => password,
    });
  }
};
