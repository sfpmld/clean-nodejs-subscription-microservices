const jwt = require('jsonwebtoken');

const sign = (payload, authSecret, subject) => {
  const signOptions = {
    algorithm: 'HS256',
    issuer: process.env.TOKEN_ISSUER,
    subject
  }
  return jwt.sign(payload, authSecret, signOptions);
};

const verify = (token, authSecret, subject) => {
  const verifyOptions = {
    algorithm: 'HS256',
    issuer: process.env.TOKEN_ISSUER,
    subject
  }
  return jwt.verify(token, authSecret, verifyOptions);
};
module.exports = Object.freeze({
  sign,
  verify
});
