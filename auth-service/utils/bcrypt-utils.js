const bcrypt = require('bcrypt');

const rounds = 10;

const hash = async (password) => {
  return await bcrypt.hash(password, rounds);
};

const check = async (password, hashPassword) => {
  return await bcrypt.compare(password, hashPassword);
};


module.exports = Object.freeze({
  check,
  hash
});
