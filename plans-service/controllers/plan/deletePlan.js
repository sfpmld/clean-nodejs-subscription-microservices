const {
  HTTP_OK,
  HTTP_BAD_REQUEST
} = require('../../nomenclatures/httpStatusCode');

module.exports = function makeDeletePlan ({ removePlan }) {
  return async function deletePlan (httpRequest) {
    const headers = {
      'Content-Type': 'application/json'
    }
    try {
      const result = await removePlan(httpRequest.params)
      return {
        headers,
        statusCode: HTTP_OK,
        body: result
      }
    } catch (e) {
      console.log(e)
      return {
        headers,
        statusCode: HTTP_BAD_REQUEST,
        body: {
          error: e.message
        }
      }
    }
  }
}
