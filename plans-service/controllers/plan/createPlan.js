const {
  HTTP_OK,
  HTTP_BAD_REQUEST
} = require('../../nomenclatures/httpStatusCode');

module.exports = function makeCreatePlan ({ addPlan }) {
  return async function createPlan (httpRequest) {
    const headers = {
      'Content-Type': 'application/json'
    }
    try {
      const plan = httpRequest.body;
      const userId = httpRequest.user;
      plan.userId = userId;

      const result = await addPlan(plan)
      return {
        headers,
        statusCode: HTTP_OK,
        body: result
      }
    } catch (e) {
      console.log(e)
      return {
        headers,
        statusCode: HTTP_BAD_REQUEST,
        body: {
          error: e.message
        }
      }
    }
  }
}
