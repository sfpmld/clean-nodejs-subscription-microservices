const {
  HTTP_OK,
  HTTP_BAD_REQUEST
} = require('../../nomenclatures/httpStatusCode');

module.exports = function makeGetPlan ({ listPlan }) {
  return async function getPlan (httpRequest) {

    const headers = {
      'Content-Type': 'application/json'
    }
    try {
      debugger
      const userId = httpRequest.user;
      const result = await listPlan(userId);
      return {
        headers,
        statusCode: HTTP_OK,
        body: result
      }
    } catch (e) {
      console.log(e)
      return {
        headers,
        statusCode: HTTP_BAD_REQUEST,
        body: {
          error: e.message
        }
      }
    }
  }
}
