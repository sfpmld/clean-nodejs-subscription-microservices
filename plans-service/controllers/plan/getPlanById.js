const {
  HTTP_OK,
  HTTP_BAD_REQUEST
} = require('../../nomenclatures/httpStatusCode');

module.exports = function makeGetPlanById ({ planById }) {
  return async function getPlanById (httpRequest) {
    const headers = {
      'Content-Type': 'application/json'
    }
    try {
      const result = await planById(httpRequest.params);
      return {
        headers,
        statusCode: HTTP_OK,
        body: result
      }
    } catch (e) {
      console.log(e)
      return {
        headers,
        statusCode: HTTP_BAD_REQUEST,
        body: {
          error: e.message
        }
      }
    }
  }
}
