const {
  listPlan,
  addPlan,
  planById,
  removePlan,
} = require('../services/plan');

const notFound = require('./notFound');

const makeGetPlanList = require('./plan/getPlanList');
const makeCreatePlan = require('./plan/createPlan');
const makeGetPlanById = require('./plan/getPlanById');
const makeDeletePlan = require('./plan/deletePlan');

const getPlanList = makeGetPlanList({ listPlan });
const createPlan = makeCreatePlan({ addPlan });
const getPlanById = makeGetPlanById({ planById });
const deletePlan = makeDeletePlan({ removePlan });


module.exports = Object.create({
  // plan
  getPlanList,
  createPlan,
  getPlanById,
  deletePlan,
  // not found
  notFound,
});

