const makeExpressCallback = require('../helpers/express-callback');
const protectedRoute = require('../middlewares/protectedRoute');
const {
  getPlanList,
  getPlanById,
  createPlan,
  deletePlan,
  notFound
} = require('../controllers');


const router = require('express').Router();

// check authaurization
router.use(protectedRoute());

// get planRoutes Requests
router.get('/', makeExpressCallback(getPlanList));

// get planRoutes Requests
router.get('/:id', makeExpressCallback(getPlanById));

// post planRoutes Requests
router.post('/', makeExpressCallback(createPlan));

// delete planRoutes Requests
router.delete('/:id', makeExpressCallback(deletePlan));

// notFound
router.use('*', makeExpressCallback(notFound));


module.exports = router;
