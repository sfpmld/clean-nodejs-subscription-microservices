module.exports = function makeRemovePlan ({ PlanDb, cachingService }) {
  return async function removePlan ({ id } = {}) {

    let result = Promise.resolve();
    // test if plans exists
    const plan = await PlanDb.findOne({ where: { id } })
    if(plan) {
      await cachingService.purgeCache(plan.userId);
      result = await PlanDb.deleteOne(id)
    }
    return result;
  }
}
