const makeListPlan = require('./listPlan');
const makeAddPlan = require('./addPlan');
const makePlanById = require('./planById');
const makeRemovePlan = require('./removePlan');

const cachingService = require('../cache');
const { PlanDb } = require('../../data-access');


const listPlan = makeListPlan({ PlanDb, cachingService });
const addPlan = makeAddPlan({ PlanDb, cachingService });
const planById = makePlanById({ PlanDb });
const removePlan = makeRemovePlan({ PlanDb, cachingService });


module.exports =  Object.create({
  listPlan,
  addPlan,
  planById,
  removePlan
});

