module.exports = function makeListPlan ({ PlanDb, cachingService }) {

  return async function listPlan (userId) {

    let plans = await cachingService.getPlans(userId)
    if (!plans) {
      plans = await PlanDb.findAll(userId);
      // storing plans in redis cache database
      await cachingService.purgeCache(userId);
      await cachingService.storePlans(userId, plans);
    }
    return plans;
  }
}
