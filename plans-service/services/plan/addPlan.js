const { makePlan } = require('../../entities')

module.exports = function makeAddPlan ({ PlanDb, cachingService }) {
  return async function addPlan (data = {}) {

    let Plan = makePlan(data)

    // purge cache before adding new plan
    await cachingService.purgeCache(Plan.getUserId);

    return await PlanDb.create({
      id: Plan.getId(),
      name: Plan.getName(),
      price: Plan.getPrice(),
      type: Plan.getType(),
      userId: Plan.getUserId(),
      createdAt: Plan.getCreatedAt(),
      updatedAt: Plan.getUpdatedAt(),
    })
  }
}
