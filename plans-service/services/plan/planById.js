module.exports = function makePlanById ({ PlanDb }) {
  return async function planById({ id } = {}) {

    const results = await PlanDb.findOne({ where: { id } })

    return results;

  }
}
