const config = require('../../config');
const makeCachingService = require('./caching');

const redisConfig = {
  redisHost: config.redisHost,
  redisPort: config.redisPort,
  redisPassport: config.redisPassport
};

const cachingService = makeCachingService(redisConfig);

module.exports = cachingService();
