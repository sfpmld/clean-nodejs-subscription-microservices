const redis = require('redis');
const promisify = require('util').promisify;

module.exports = function makeCachingService(redisConfig) {

  return function cachingService() {
    const planKey = 'plans';
    const redisClient = redis.createClient({
      host: redisConfig.host,
      port: redisConfig.port,
      auth_pass: redisConfig.pasword
    });

    const getUserKey = function(id) {
      return `user#${id}`;
    };

    const getPlans = async function(userId) {
      const plans = await promisify(redisClient.hget).bind(redisClient)(getUserKey(userId), planKey);
      return plans ? JSON.parse(plans) : null;
    };

    const purgeCache = async function(userId) {
      if (!userId) {
        return Promise.resolve();
      }
      return promisify(redisClient.expire).bind(redisClient)(getUserKey(userId), 0);
    };

    const storePlans = async function(userId, plans) {
      plans = plans || [];
      await promisify(redisClient.hset).bind(redisClient)(getUserKey(userId), planKey, JSON.stringify(plans));
    };

    return Object.create({
      getPlans,
      storePlans,
      purgeCache
    });
  };
};
