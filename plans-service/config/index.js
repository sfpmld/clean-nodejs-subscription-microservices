module.exports = {
  // App
  PORT: process.env.PORT,
  API_PREFIX: process.env.API_PREFIX || '/api/v1',
  // Sequelize DB vars
  host: process.env.DB_HOST,
  database: process.env.DB_NAME,
  username: process.env.DB_USERNAME,
  password: process.env.DB_USER_PASSWORD,
  port: process.env.DB_PORT,
  dialect: process.env.DB_DIALECT,
  operatorsaliases: process.env.DB_OPERATORS_ALIASES,
  redisHost: process.env.REDIS_HOST,
  redisPort: process.env.REDIS_PORT,
  redisPassport: process.env.REDIS_PASSWORD
}
