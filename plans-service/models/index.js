const { Plan } = require('./plan');
const { PlanValidationSchema } = require('./plan');


module.exports = Object.freeze({
  Plan,
  PlanValidationSchema,
});
