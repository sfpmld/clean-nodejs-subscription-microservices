const makeDbConnection = require('./db/connection');
const { Sequelize, SequelizeClient } = makeDbConnection();

const makePlanModel = require('../models').Plan;

const makePlanDb = require('./planDb');

const makeDb = () => {
  Plan = makePlanModel(SequelizeClient, Sequelize);

  return Object.freeze({
    Plan,
  })
}

const PlanDb = makePlanDb(makeDb);

module.exports = Object.freeze({
  PlanDb,
});
