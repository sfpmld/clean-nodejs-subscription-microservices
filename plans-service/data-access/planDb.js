const Id = require('../utils/makeId');


module.exports = function makePlanDb(makeDb) {
  const db = makeDb();

  const findAll = function(userId) {
    return db.Plan.findAll({ where: { userId } });
  };

  const findOne = function(id) {
    return db.Plan.findOne(id)
  }

  const create = function(data) {
    return db.Plan.create(data)
  }

  const deleteOne = async function(id) {
    const planToDelete = await db.Plan.findOne({ where: {id} })
    return await planToDelete.destroy();
  }

  return Object.freeze({
    findAll,
    findOne,
    create,
    deleteOne
  });
};



