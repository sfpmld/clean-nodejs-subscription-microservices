const express = require('express');
const router = express.Router();
const app = express();

const api_prefix = require('./config')['API_PREFIX']

const commonMiddlewares = require('../middlewares/common-middlewares');
const authMiddleware = require('./middlewares/auth');
const mainErrorHandler = require('../middlewares/errors-handler');

const planRoutes = require('./routes/planRoutes');



// Common Middlewares
commonMiddlewares(app);

// Authorization Middleware
authMiddleware(app);

// Routes Middlewares
app.use('', planRoutes);

// Main Error Handling Block
mainErrorHandler(app)


module.exports = app;
