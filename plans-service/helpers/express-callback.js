function makeExpressCallback(controller) {
  return (req, res, next) => {
    const httpRequest = {
      path: req.path,
      originalUrl: req.originalUrl,
      method: req.method,
      params: req.params,
      query: req.query,
      body: req.body,
      user: req.user,
      headers: {
        "Content-Type": req.get("Content-Type"),
        Referer: req.get("referer"),
        "User-Agent": req.get("User-Agent")
      }
    };

    controller(httpRequest)
      .then(httpResponse => {
        if (httpResponse.headers) {
          res.set(httpResponse.headers);
        }

        // sending response to client
        res.status(httpResponse.statusCode).json(httpResponse.body);
      })
      .catch(err => next(err));
  };
}

module.exports = makeExpressCallback;
