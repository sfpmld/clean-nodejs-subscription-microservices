const validate = require('../helpers/validate');
const Id = require('../utils/makeId');
const buildMakePlan = require('./plan');


const makePlan = buildMakePlan({ Id, validate });

module.exports = {
  makePlan,
};
