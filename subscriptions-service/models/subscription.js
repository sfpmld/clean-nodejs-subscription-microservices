const Joi = require('@hapi/joi');
'use strict';

const SubscriptionValidationSchema = Joi.object().keys({
  planId: Joi.string().guid().required().error(() => 'your error message'),
  coupon: Joi.number().min(0).max(100).optional().allow(null),
  cardNumber: Joi.string().creditCard().required(),
  holderName: Joi.string().required(),
  expirationDate: Joi.string().required(),
  cvv: Joi.string().min(3).max(3).required(),
  userId: Joi.string().guid().required(),
});

const Subscription = (sequelize, DataTypes) => {
  const Subscription = sequelize.define('Subscription', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      allowNull: false,
    },
    planId: {
      type: DataTypes.UUID,
      allowNull: false
    },
    coupon: {
      type: DataTypes.STRING,
      allowNull: true
    },
    cardNumber: {
      type: DataTypes.STRING,
      allowNull: false
    },
    holderName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    expirationDate: {
      type: DataTypes.STRING,
      allowNull: false
    },
    cvv: {
      type: DataTypes.STRING,
      allowNull: false
    },
    userId: {
      type: DataTypes.UUID,
      allowNull: false
    }
  }, {});
  Subscription.associate = function(models) {
    // associations can be defined here
  };
  return Subscription;
};

module.exports = Object.freeze({
  Subscription,
  SubscriptionValidationSchema
});
