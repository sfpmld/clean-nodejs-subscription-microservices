const express = require('express');
const router = express.Router();
const app = express();

const api_prefix = require('./config')['API_PREFIX']

const commonMiddlewares = require('../middlewares/common-middlewares');
const AuthMiddleware = require('./middlewares/auth');
const mainErrorHandler = require('../middlewares/errors-handler');

const subscriptionRoutes = require('./routes/subscriptionRoutes');



// Common Middlewares
commonMiddlewares(app);

// Authorization
AuthMiddleware(app);

// Routes Middlewares
app.use('', subscriptionRoutes);

// Main Error Handling Block
mainErrorHandler(app)


module.exports = app;
