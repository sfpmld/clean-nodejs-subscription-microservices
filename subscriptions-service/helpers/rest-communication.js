const axios = require('axios');

module.exports = function fetch () {

  async function get(url) {
    return await axios.default.get(url);
  };

  async function post(url, body) {
    return await axios.post(url, body);
  };

  return Object.freeze({
    get,
    post,
  });
};
