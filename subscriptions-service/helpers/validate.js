const Joi = require("@hapi/joi");

const { SubscriptionValidationSchema } = require('../models');
// const ErrorHandlingMiddleware = require('../errors/error-handling');


let validators = {
  "Subscription": {
    scopes: {
      default: SubscriptionValidationSchema
    }
  }
}

function scopeExists(validator, scope) {
  return Object.keys(validator.scopes).find(key => key === scope) != undefined
}

function getSchema(model, scope) {
  let validator = validators[model]
  if(!validator) {
    throw new Error("Validator doesn't exist");
  }

  // Check if the given validator has multiple scopes
  if(validator.scopes) {
    if(scope) {
      if(!scopeExists(validator, scope)) {
        throw new Error(`Scope ${scope} doesn't exist in ${model} validator `);
      } else {
        return validator.scopes[scope]
      }
    } else {
      return validator.scopes.default;
    }
  } else {
    return validator;
  }
}

module.exports = function (model, object, scope) {
  return getSchema(model, scope).validate(object, {
    allowKnown: true
  });
};
