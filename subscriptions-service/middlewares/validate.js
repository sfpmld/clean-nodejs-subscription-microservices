const chalk = require('chalk')
const accessDeniedError = require('../errors');
const authenticationError = require('../errors');
const validationError = require('../errors');
const {
  HTTP_VALIDATION_ERROR,
  HTTP_AUTHENTICATION_ERROR,
  HTTP_ACCESS_DENIED
} = require('../nomenclatures/httpStatusCode');


const errorLogger = (error, req, res, next) => {
  if(error.message) {
    console.log(chalk.red(error.message));
  };
  if(error.stack) {
    console.log(chalk.red(error.stack));
  };
};

const validationErrorHandler = (error, req, res, next) => {
  if(error instanceof validationError) {
    return res.sendStatus(HTTP_VALIDATION_ERROR) ;
  };
  next(error);
};

const authenticationErrorHandler = (error, req, res, next) => {
  if(error instanceof authenticationError) {
    return res.sendStatus(HTTP_AUTHENTICATION_ERROR);
  };
  next(error);
};

const accessDeniedErrorHandler = (error, req, res, next) => {
  if(error instanceof accessDeniedError) {
    return res.sendStatus(HTTP_ACCESS_DENIED);
  };
  next(error);
};

const genericErrorHandler = (error, req, res, next) => {
  const statusCode = error.status || HTTP_INTERNAL_ERROR;
  const message = error.status === HTTP_INTERNAL_ERROR ? HTTP_INTERNAL_ERROR : error.message;

  res.sendStatus(statusCode).json({
    statusCode,
    data: {},
    message,
    error: {
      message,
      status: statusCode
    }
  });
  next();
}

module.exports = makeErrorHandlingMiddleware = app => {
  app.use([
    errorLogger,
    validationErrorHandler,
    accessDeniedErrorHandler,
    authenticationErrorHandler,
    genericErrorHandler
  ]);
};
