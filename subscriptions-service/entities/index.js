const validate = require('../helpers/validate');
const Id = require('../utils/makeId');
const buildMakeSubscription = require('./subscription');


const makeSubscription = buildMakeSubscription({ Id, validate });

module.exports = {
  makeSubscription,
};
