const notFound = require('./notFound');

const {
  listSubscription,
  addSubscription,
  subscriptionById,
  removeSubscription,
} = require('../services/subscription');

const makeGetSubscriptionList = require('./subscription/getSubscriptionList');
const makeCreateSubscription = require('./subscription/createSubscription');
const makeGetSubscriptionById = require('./subscription/getSubscriptionById');
const makeDeleteSubscription = require('./subscription/deleteSubscription');

const getSubscriptionList = makeGetSubscriptionList({ listSubscription });
const createSubscription  = makeCreateSubscription({ addSubscription });
const getSubscriptionById = makeGetSubscriptionById({ subscriptionById });
const deleteSubscription = makeDeleteSubscription({ removeSubscription });


module.exports = Object.create({
  // subscription
  getSubscriptionList,
  createSubscription,
  getSubscriptionById,
  deleteSubscription,
  // not found
  notFound,
});

