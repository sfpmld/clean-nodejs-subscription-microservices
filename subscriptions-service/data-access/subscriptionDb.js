const { ValidationError } = require('../../errors');

module.exports = function makeSubscriptionDb(makeDb, makeFetch, addPayment) {

  const db = makeDb();
  const fetch = makeFetch();

  const findAll = async function() {
    return await db.Subscription.findAll();
  };

  const findOne = async function(id) {
    return await db.Subscription.findOne(id);
  }

  const create = async function(data) {
    // retrieve Plan information in aim to create Subscription
    const response = await fetch.get(process.env.PLAN_API_URL +'/'+ data.planId);

    const plan = response.data;
    if (!plan) {
      throw new ValidationError('Given plan is invalid.');
    };
    const subscription = await db.Subscription.create(data)

    const payment = await addPayment;
    payment.init();

    return await payment.sendMessage(JSON.stringify({ plan, subscription }));
  }

  const deleteOne = async function(id) {

    const subscriptionToDelete = await db.Subscription.findOne({ id });
    return await subscriptionToDelete.destroy();
  }

  return Object.freeze({
    findAll,
    findOne,
    create,
    deleteOne
  });
};




