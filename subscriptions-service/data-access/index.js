const makeFetch = require('../helpers/rest-communication');
const { addPayment } = require('../amqp')
const makeDbConnection = require('./db/connection');
const { Sequelize, SequelizeClient } = makeDbConnection();

const makeSubscriptionModel = require('../models').Subscription;

const makeSubscriptionDb = require('./subscriptionDb');

const makeDb = () => {
  Subscription = makeSubscriptionModel(SequelizeClient, Sequelize);

  return Object.freeze({
    Subscription
  })
}

const SubscriptionDb = makeSubscriptionDb(makeDb, makeFetch, addPayment);

module.exports = Object.freeze({
  SubscriptionDb
});
