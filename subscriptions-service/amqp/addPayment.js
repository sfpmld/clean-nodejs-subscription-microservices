const amqplib = require('amqplib');


module.exports = function buildMakeAddPayment() {

  return async function makeAddPayment(connectionString, channelName, queueName) {

    const connection = await amqplib.connect(connectionString);
    const channel = await connection.createChannel(channelName);

    return function addPayment () {

      async function sendMessage(message) {
        return await channel.sendToQueue(queueName, Buffer.from(message));
      }

      async function init() {
        console.log(`Initializing RabbitMQ connection to ${connectionString}`);
        // check queue avaibility
        await channel.assertQueue(queueName);
      }

      return Object.create({
        init,
        sendMessage,
      })
    };
  };
}

