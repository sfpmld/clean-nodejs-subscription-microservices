const buildMakeAddPayment = require('./addPayment');

const makeAddPayment = buildMakeAddPayment();

const CONNECTION_STRING = process.env.AMQP_CONNECTION_STRING;
const CHANNEL_NAME = process.env.AMQP_CHANNEL_NAME;
const QUEUE_NAME = process.env.AMQP_QUEUE_NAME;

const addPayment = makeAddPayment(CONNECTION_STRING, CHANNEL_NAME, QUEUE_NAME)
  .then(service => service())
  .catch(error => {
    throw new Error(error);
  });


module.exports = Object.freeze({
  addPayment,
});
